package org.hnau.db.jdbc

import org.hnau.db.base.table.Column
import org.hnau.orm.ExtractInfo
import sun.misc.ExtensionInfo


fun <K, T> JdbcExtractInfo(
        keys: Iterable<K>,
        extract: JdbcValueProvider<K>.() -> T
) = ExtractInfo<K, T, JdbcValueProvider<K>>(
        keys = keys,
        extract = extract
)