package org.hnau.db.jdbc

import org.hnau.base.extensions.checkNullable
import java.sql.PreparedStatement
import java.sql.SQLType
import java.sql.Types


typealias JdbcValueReceiver = (PreparedStatement.(index: Int) -> Unit) -> Unit

fun JdbcValueReceiver.setByte(
        value: Byte
) = invoke { index ->
    setByte(index, value)
}

fun JdbcValueReceiver.setShort(
        value: Short
) = invoke { index ->
    setShort(index, value)
}

fun JdbcValueReceiver.setInt(
        value: Int
) = invoke { index ->
    setInt(index, value)
}

fun JdbcValueReceiver.setLong(
        value: Long
) = invoke { index ->
    setLong(index, value)
}

fun JdbcValueReceiver.setFloat(
        value: Float
) = invoke { index ->
    setFloat(index, value)
}

fun JdbcValueReceiver.setDouble(
        value: Double
) = invoke { index ->
    setDouble(index, value)
}

fun JdbcValueReceiver.setBoolean(
        value: Boolean
) = invoke { index ->
    setBoolean(index, value)
}

fun JdbcValueReceiver.setString(
        value: String
) = invoke { index ->
    setString(index, value)
}

fun JdbcValueReceiver.setBytes(
        value: ByteArray
) = invoke { index ->
    setBytes(index, value)
}

fun <T> JdbcValueReceiver.setCustom(
        value: T
) = invoke { index ->
    setObject(index, value)
}

fun JdbcValueReceiver.setNull() = invoke { index ->
    setNull(index, Types.NULL)
}

inline fun <T> JdbcValueReceiver.setValueOrNull(
        value: T?,
        setValue: (T) -> Unit
) = value.checkNullable(
        ifNull = { setNull() },
        ifNotNull = { existenceValue -> setValue(existenceValue) }
)

fun JdbcValueReceiver.setByteOrNull(
        value: Byte?
) = setValueOrNull(
        value = value,
        setValue = ::setByte
)

fun JdbcValueReceiver.setShortOrNull(
        value: Short?
) = setValueOrNull(
        value = value,
        setValue = ::setShort
)

fun JdbcValueReceiver.setIntOrNull(
        value: Int?
) = setValueOrNull(
        value = value,
        setValue = ::setInt
)

fun JdbcValueReceiver.setLongOrNull(
        value: Long?
) = setValueOrNull(
        value = value,
        setValue = ::setLong
)

fun JdbcValueReceiver.setFloatOrNull(
        value: Float?
) = setValueOrNull(
        value = value,
        setValue = ::setFloat
)

fun JdbcValueReceiver.setDoubleOrNull(
        value: Double?
) = setValueOrNull(
        value = value,
        setValue = ::setDouble
)

fun JdbcValueReceiver.setBooleanOrNull(
        value: Boolean?
) = setValueOrNull(
        value = value,
        setValue = ::setBoolean
)

fun JdbcValueReceiver.setStringOrNull(
        value: String?
) = setValueOrNull(
        value = value,
        setValue = ::setString
)

fun JdbcValueReceiver.setBytesOrNull(
        value: ByteArray?
) = setValueOrNull(
        value = value,
        setValue = ::setBytes
)

fun <T> JdbcValueReceiver.setCustomOrNull(
        value: T?
) = setValueOrNull(
        value = value,
        setValue = { invoke { index -> setObject(index, value) } }
)