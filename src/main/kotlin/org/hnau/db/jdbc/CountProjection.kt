package org.hnau.db.jdbc

data class CountProjection(
        val count: Int
) {

    companion object {

        val columns = listOf("COUNT(*)")

        val extract: JdbcValueProvider<String>.() -> CountProjection = {
            CountProjection(getInt("count"))
        }

    }

}