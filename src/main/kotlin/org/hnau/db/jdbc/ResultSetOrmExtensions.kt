package org.hnau.db.jdbc

import org.hnau.base.data.associator.Associator
import org.hnau.base.data.associator.byHashMap
import org.hnau.base.data.associator.toAutoAssociator
import org.hnau.orm.iterator.ExtractorCachingIterable
import org.hnau.orm.iterator.ExtractorOnceIterable
import org.hnau.orm.iterator.IterateInfo
import java.sql.ResultSet


inline fun <K> ResultSet.toJdbcValueProvider(
        crossinline extractColumnName: (K) -> String
) = object : JdbcValueProvider<K> {

    private val columnsIndexesGetter = Associator
            .byHashMap<K, Int>()
            .toAutoAssociator { key -> findColumn(extractColumnName(key)) }


    override fun <T> get(
            key: K,
            getter: ResultSet.(columnIndex: Int) -> T
    ) = getter(
            columnsIndexesGetter(key)
    )

}

@PublishedApi
internal inline fun <K, T> ResultSet.toEntityExtractor(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: JdbcValueProvider<K>.() -> T
): () -> T = toJdbcValueProvider(
        extractColumnName = extractColumnName
).run { { entityBuilder() } }

@PublishedApi
internal inline fun <K, T> ResultSet.toIterateInfo(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: JdbcValueProvider<K>.() -> T
) = IterateInfo(
        switchLine = ::next,
        close = ::close,
        extract = toEntityExtractor(extractColumnName, entityBuilder)
)

inline fun <K, T, R> ResultSet.iterate(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: JdbcValueProvider<K>.() -> T,
        handle: Iterable<T>.() -> R
) = ExtractorOnceIterable(
        iterateInfo = toIterateInfo(extractColumnName, entityBuilder)
).use(handle)

inline fun <K, T, R> ResultSet.iterateCached(
        crossinline extractColumnName: (K) -> String,
        crossinline entityBuilder: JdbcValueProvider<K>.() -> T,
        handle: Iterable<T>.() -> R
) = ExtractorCachingIterable(
        iterateInfo = toIterateInfo(extractColumnName, entityBuilder)
).use(handle)