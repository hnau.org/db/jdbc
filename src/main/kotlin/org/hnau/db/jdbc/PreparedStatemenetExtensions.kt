package org.hnau.db.jdbc

import org.hnau.db.base.sql.ParametrizedSql
import org.hnau.db.base.sql.ParamsInjector
import org.hnau.db.jdbc.utils.use
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet


inline fun JdbcParametrizedSql(
        build: ParamsInjector<JdbcValueReceiver>.() -> String
) = ParametrizedSql<JdbcValueReceiver>(
        build
)


inline fun <R> Connection.sql(
        sql: ParametrizedSql<JdbcValueReceiver>,
        block: PreparedStatement.() -> R
) = prepareStatement(sql.sql).use { statement ->
    sql.params.forEachIndexed { index, param ->
        param { preparedStatementParamSetter ->
            statement.preparedStatementParamSetter(index + 1)
        }
    }
    statement.block()
}


inline fun <R> PreparedStatement.query(
        handleResult: ResultSet.() -> R
) = executeQuery().use(handleResult)