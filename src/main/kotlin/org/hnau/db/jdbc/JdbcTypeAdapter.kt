package org.hnau.db.jdbc

import org.hnau.orm.TypeAdapter


fun <K, T> JdbcTypeAdapter(
        read: JdbcValueProvider<K>.(K) -> T,
        write: JdbcValueReceiver.(K, T) -> Unit
) = TypeAdapter<K, T, JdbcValueReceiver, JdbcValueProvider<K>>(
        read = read,
        write = write
)

object JdbcTypeAdapters

val TypeAdapter.Companion.jdbc
    get() = JdbcTypeAdapters

fun <K> JdbcTypeAdapters.byte() = JdbcTypeAdapter<K, Byte>(
        read = { key -> getByte<K>(key) },
        write = { _, value -> setByte(value) }
)

fun <K> JdbcTypeAdapters.short() = JdbcTypeAdapter<K, Short>(
        read = { key -> getShort<K>(key) },
        write = { _, value -> setShort(value) }
)

fun <K> JdbcTypeAdapters.int() = JdbcTypeAdapter<K, Int>(
        read = { key -> getInt<K>(key) },
        write = { _, value -> setInt(value) }
)

fun <K> JdbcTypeAdapters.long() = JdbcTypeAdapter<K, Long>(
        read = { key -> getLong<K>(key) },
        write = { _, value -> setLong(value) }
)

fun <K> JdbcTypeAdapters.float() = JdbcTypeAdapter<K, Float>(
        read = { key -> getFloat<K>(key) },
        write = { _, value -> setFloat(value) }
)

fun <K> JdbcTypeAdapters.double() = JdbcTypeAdapter<K, Double>(
        read = { key -> getDouble<K>(key) },
        write = { _, value -> setDouble(value) }
)

fun <K> JdbcTypeAdapters.boolean() = JdbcTypeAdapter<K, Boolean>(
        read = { key -> getBoolean<K>(key) },
        write = { _, value -> setBoolean(value) }
)

fun <K> JdbcTypeAdapters.string() = JdbcTypeAdapter<K, String>(
        read = { key -> getString<K>(key) },
        write = { _, value -> setString(value) }
)

fun <K> JdbcTypeAdapters.bytes() = JdbcTypeAdapter<K, ByteArray>(
        read = { key -> getBytes<K>(key) },
        write = { _, value -> setBytes(value) }
)

inline fun <K, reified T> JdbcTypeAdapters.custom() = JdbcTypeAdapter<K, T>(
        read = { key -> getCustom<K, T>(key) },
        write = { _, value -> setCustom(value) }
)

fun <K> JdbcTypeAdapters.byteOrNull() = JdbcTypeAdapter<K, Byte?>(
        read = { key -> getByteOrNull<K>(key) },
        write = { _, value -> setByteOrNull(value) }
)

fun <K> JdbcTypeAdapters.shortOrNull() = JdbcTypeAdapter<K, Short?>(
        read = { key -> getShortOrNull<K>(key) },
        write = { _, value -> setShortOrNull(value) }
)

fun <K> JdbcTypeAdapters.intOrNull() = JdbcTypeAdapter<K, Int?>(
        read = { key -> getIntOrNull<K>(key) },
        write = { _, value -> setIntOrNull(value) }
)

fun <K> JdbcTypeAdapters.longOrNull() = JdbcTypeAdapter<K, Long?>(
        read = { key -> getLongOrNull<K>(key) },
        write = { _, value -> setLongOrNull(value) }
)

fun <K> JdbcTypeAdapters.floatOrNull() = JdbcTypeAdapter<K, Float?>(
        read = { key -> getFloatOrNull<K>(key) },
        write = { _, value -> setFloatOrNull(value) }
)

fun <K> JdbcTypeAdapters.doubleOrNull() = JdbcTypeAdapter<K, Double?>(
        read = { key -> getDoubleOrNull<K>(key) },
        write = { _, value -> setDoubleOrNull(value) }
)

fun <K> JdbcTypeAdapters.booleanOrNull() = JdbcTypeAdapter<K, Boolean?>(
        read = { key -> getBooleanOrNull<K>(key) },
        write = { _, value -> setBooleanOrNull(value) }
)

fun <K> JdbcTypeAdapters.stringOrNull() = JdbcTypeAdapter<K, String?>(
        read = { key -> getStringOrNull<K>(key) },
        write = { _, value -> setStringOrNull(value) }
)

fun <K> JdbcTypeAdapters.bytesOrNull() = JdbcTypeAdapter<K, ByteArray?>(
        read = { key -> getBytesOrNull<K>(key) },
        write = { _, value -> setBytesOrNull(value) }
)

inline fun <K, reified T> JdbcTypeAdapters.customOrNull() = JdbcTypeAdapter<K, T?>(
        read = { key -> getObjectOrNull<K, T>(key) },
        write = { _, value -> setCustomOrNull(value) }
)