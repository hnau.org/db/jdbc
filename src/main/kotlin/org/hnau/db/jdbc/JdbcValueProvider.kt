package org.hnau.db.jdbc

import org.hnau.base.extensions.boolean.ifFalse
import java.sql.ResultSet

interface JdbcValueProvider<K> {

    fun <T> get(
            key: K,
            getter: ResultSet.(keyIndex: Int) -> T
    ): T

}

fun <K> JdbcValueProvider<K>.getByte(
        key: K
) = get(key) { keyIndex ->
    getByte(keyIndex)
}

fun <K> JdbcValueProvider<K>.getShort(
        key: K
) = get(key) { keyIndex ->
    getShort(keyIndex)
}

fun <K> JdbcValueProvider<K>.getInt(
        key: K
) = get(key) { keyIndex ->
    getInt(keyIndex)
}

fun <K> JdbcValueProvider<K>.getLong(
        key: K
) = get(key) { keyIndex ->
    getLong(keyIndex)
}

fun <K> JdbcValueProvider<K>.getFloat(
        key: K
) = get(key) { keyIndex ->
    getFloat(keyIndex)
}

fun <K> JdbcValueProvider<K>.getDouble(
        key: K
) = get(key) { keyIndex ->
    getDouble(keyIndex)
}

fun <K> JdbcValueProvider<K>.getBoolean(
        key: K
) = get(key) { keyIndex ->
    getBoolean(keyIndex)
}

fun <K> JdbcValueProvider<K>.getString(
        key: K
): String = get(key) { keyIndex ->
    getString(keyIndex)
}

fun <K> JdbcValueProvider<K>.getBytes(
        key: K
): ByteArray = get(key) { keyIndex ->
    getBytes(keyIndex)
}

inline fun <K, reified T> JdbcValueProvider<K>.getCustom(
        key: K
): T = get(key) { keyIndex ->
    getObject(keyIndex, T::class.java)
}

inline fun <K, T> JdbcValueProvider<K>.getValueOrNull(
        key: K,
        crossinline getValue: ResultSet.(keyIndex: Int) -> T?
) = get(key) { keyIndex ->
    val valueOrNull = getValue(keyIndex)
    wasNull().ifFalse { valueOrNull }
}

fun <K> JdbcValueProvider<K>.getByteOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getByte(keyIndex)
}

fun <K> JdbcValueProvider<K>.getShortOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getShort(keyIndex)
}

fun <K> JdbcValueProvider<K>.getIntOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getInt(keyIndex)
}

fun <K> JdbcValueProvider<K>.getLongOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getLong(keyIndex)
}

fun <K> JdbcValueProvider<K>.getFloatOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getFloat(keyIndex)
}

fun <K> JdbcValueProvider<K>.getDoubleOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getDouble(keyIndex)
}

fun <K> JdbcValueProvider<K>.getBooleanOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getBoolean(keyIndex)
}

fun <K> JdbcValueProvider<K>.getStringOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getString(keyIndex)
}

fun <K> JdbcValueProvider<K>.getBytesOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getBytes(keyIndex)
}

inline fun <K, reified T> JdbcValueProvider<K>.getObjectOrNull(
        key: K
) = getValueOrNull(key) { keyIndex ->
    getObject(keyIndex, T::class.java)
}