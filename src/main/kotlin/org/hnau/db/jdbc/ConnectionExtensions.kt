package org.hnau.db.jdbc

import org.hnau.base.extensions.checkNullable
import org.hnau.base.utils.tryOrElse
import java.sql.Connection
import java.sql.Savepoint


fun <R> Connection.transaction(
        id: String? = null,
        block: Connection.() -> R
): R {
    val storedAutoCommit = autoCommit
    autoCommit = false
    val savepoint: Savepoint = id.checkNullable(
            ifNull = ::setSavepoint,
            ifNotNull = ::setSavepoint
    )
    return tryOrElse(
            action = {
                block().also { commit() }
            },
            onThrow = { throwable ->
                rollback(savepoint)
                throw throwable
            },
            finally = {
                autoCommit = storedAutoCommit
            }
    )
}